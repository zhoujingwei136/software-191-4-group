package com.system.colleges.controller;

import com.system.colleges.entity.CollegeMajors;
import com.system.colleges.service.CollegeMajorsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@ResponseBody
@CrossOrigin
@Slf4j
@RequestMapping("/college")
public class CollegeMajorsController {

    @Autowired
    CollegeMajorsService service;

    @GetMapping("/queryAllMajorsByCollege")
    public List<CollegeMajors> queryAllMajorsByCollege(String cid){
        List<CollegeMajors> cm = service.queryAllMajorsByCollege(cid);
        return cm;
    }

    @GetMapping("/queryCollegeMajorById")
    public CollegeMajors queryCollegeMajorById(String id){
        CollegeMajors cm = service.queryCollegeMajorById(id);
        String realType = service.getRealType(cm);
        cm.setType(realType);
        return cm;
    }

    @GetMapping("/queryEmphasisedByCid")
    public List<CollegeMajors> queryEmphasisedByCid(String cid){
        List<CollegeMajors> cm = service.queryEmphasisedByCid(cid);
        return cm;
    }

}
