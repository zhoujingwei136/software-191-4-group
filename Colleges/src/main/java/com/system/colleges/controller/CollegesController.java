package com.system.colleges.controller;

import com.system.colleges.entity.Colleges;
import com.system.colleges.service.CollegesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@ResponseBody
@RequestMapping("/colleges")
@CrossOrigin
@Slf4j
public class CollegesController {

    @Autowired
    CollegesService service;

    @GetMapping("/queryAllColleges")
    public List<Colleges> queryAllColleges(){
        return service.queryAllColleges();
    }

    @GetMapping("/queryCollegeById")
    public Colleges queryCollegeById(String id){
//        System.out.println(id);
//        System.out.println(service.queryCollegeById(id));
        return service.queryCollegeById(id);
    }

    @GetMapping("/queryAllName")
    public List<String> queryAllCollegesName(){
        return service.queryAllCollegesName();
    }

    @GetMapping("/findByPage")
    public Map<String, Object> findByPage(Integer page, Integer rows, String region, String type){
//        System.out.println(region);
//        System.out.println(type);
        page = page==null?1:page;
        rows = rows==null?4:rows;
        HashMap<String,Object> map = new HashMap<>();
        List<Colleges> colleges = new ArrayList<>();
        Integer totals = 0;
        int totalPage ;
        if(type==null && region==null){
            //分页处理
            colleges = service.findCollegesByPage(page,rows);
            //计算总页数
            totals = service.findTotals();
        }else {
            if (type!="" && region==""){
                colleges = service.findCollegesByPageAndType(page,rows,type);
                //计算总页数
                totals = service.findTotalsByType(type);
                totalPage = totals%rows == 0?totals/rows:totals/rows+1;
            }if(region!="" && type==""){
                colleges = service.findCollegesByPageAndRegion(page,rows,region);
                //计算总页数
                totals = service.findTotalsByRegion(region);
                totalPage = totals%rows == 0?totals/rows:totals/rows+1;
            }if(region!="" && type!=""){
                colleges = service.findCollegesByTwo(page,rows,type,region);
                //计算总页数
                totals = service.findTotalsByTwo(type,region);
            }
        }
        totalPage = totals%rows == 0?totals/rows:totals/rows+1;
        map.put("colleges",colleges);
        map.put("totals",totals);
        map.put("totalPage",totalPage);
        map.put("page",page);
//        System.out.println(map.toString());
        return map;
    }

    @GetMapping("/deleteCollege")
    public void deleteCollege(String id){
        service.deleteCollege(id);
    }

    @PostMapping("/addCollege")
    public void addCollege(@RequestBody Colleges colleges){
        System.out.println("addCollege接收到的college对象:   "+colleges);
        service.addCollege(colleges);
    }

    @PostMapping("/updateCollege")
    public void updateCollege(@RequestBody Colleges colleges){
        System.out.println("updateCollege接收到的college对象:   "+colleges);
        service.updateCollege(colleges);
    }

    @GetMapping("/queryFourAllColleges")
    public List<Colleges> queryFourAllColleges(){
        List<Colleges> l = service.queryAllColleges();
        List<Colleges> l1 = new ArrayList<>();
        for(int i=0;i<4;i++){
            l1.add(l.get(i));
        }
        return l1;
    }

}
