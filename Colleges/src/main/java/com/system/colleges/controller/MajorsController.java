package com.system.colleges.controller;

import com.system.colleges.entity.BigType;
import com.system.colleges.entity.Majors;
import com.system.colleges.entity.Types;
import com.system.colleges.service.BigTypeService;
import com.system.colleges.service.MajorsService;
import com.system.colleges.service.TypesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Controller
@ResponseBody
@RequestMapping("/majors")
@CrossOrigin
public class MajorsController {

    @Autowired
    MajorsService service;

    @GetMapping("/queryMajorById")
    public Majors queryMajorById(String id){
        return service.queryMajorById(id);
    }

    @GetMapping("/queryMajorByType")
    public List<Majors> queryMajorByType(String type){
        return service.queryMajorByType(type);
    }

    @GetMapping("/queryMajorByName")
    public Majors queryMajorByName(String name){
        return service.queryMajorByName(name);
    }

    @Autowired
    TypesService typesService;
    @Autowired
    BigTypeService btService;

    @GetMapping("/queryAllTypes")
    public List<String> queryAllTypes(){
        HashSet<String> types = new HashSet<>();
        List<Majors> m = service.queryAllMajors();
        for(int i=0;i<m.size();i++){
            types.add(m.get(i).getType());
        }
//        System.out.println(types);
        String[] ty = new String[types.size()];
        types.toArray(ty);
//        System.out.println(Arrays.toString(ty));
        types.toArray(ty);
        //        System.out.println(tys);
        return new ArrayList<>(Arrays.asList(ty));
    }

    @GetMapping("/queryByType")
    public List<Types> queryByType(String type){
        return typesService.queryAllByBig(type);
    }

    @GetMapping("/queryAllBig")
    public List<BigType> queryAllBigType(){
        return btService.queryAll();
    }

    @GetMapping("/queryAllMajors")
    public List<Majors> queryAllMajors(){
        return service.queryAllMajors();
    }

    @GetMapping("/deleteMajors")
    public void deleteMajors(String id){
        service.deleteMajors(id);
    }

}
