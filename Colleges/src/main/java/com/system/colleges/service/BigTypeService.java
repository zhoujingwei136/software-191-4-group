package com.system.colleges.service;

import com.system.colleges.entity.BigType;
import com.system.colleges.mapper.BigTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BigTypeService {

    @Autowired
    BigTypeMapper mapper;

    public List<BigType> queryAll(){
        return mapper.queryAll();
    }

}
