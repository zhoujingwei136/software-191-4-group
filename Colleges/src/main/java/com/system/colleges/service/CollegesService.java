package com.system.colleges.service;

import com.system.colleges.entity.Colleges;
import com.system.colleges.mapper.CollegesMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollegesService {

    @Autowired
    CollegesMapper mapper;

    public List<Colleges> queryAllColleges(){
        return mapper.queryAllColleges();
    }

    public Colleges queryCollegeById(String id){
        return mapper.queryCollegeById(id);
    }

    public List<Colleges> findCollegesByPage(Integer page, Integer rows){
        int start = (page-1)*rows;
        return mapper.findCollegesByPage(start,rows);
    }

    public Integer findTotals(){
        return mapper.findTotals();
    }

    public List<Colleges> findCollegesByPageAndType(Integer page, Integer rows, String type){
        int start = (page-1)*rows;
        return mapper.findCollegesByPageAndType(start,rows, type);
    }
    public Integer findTotalsByType(String type){
        return mapper.findTotalsByType(type);
    }

    public List<Colleges> findCollegesByPageAndRegion(Integer page, Integer rows, String region){
        int start = (page-1)*rows;
        return mapper.findCollegesByPageAndRegion(start,rows, region);
    }
    public Integer findTotalsByRegion(String region){
        return mapper.findTotalsByRegion(region);
    }

    public List<Colleges> findCollegesByTwo(Integer page, Integer rows, String type, String region){
        int start = (page-1)*rows;
        return mapper.findCollegesByTwo(start,rows,type,region);
    }
    public Integer findTotalsByTwo(String type, String region){
        return mapper.findTotalsByTwo(type,region);
    }

    public List<String> queryAllCollegesName(){
        return mapper.queryAllCollegesName();
    }

    public void deleteCollege(String id){
        mapper.deleteCollege(id);
    }

    public void updateCollege(Colleges colleges){
        mapper.updateCollege(colleges);
    }

    public void addCollege(Colleges colleges){
        mapper.addCollege(colleges);
    }

}
