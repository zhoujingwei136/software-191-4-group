package com.system.colleges.service;

import com.system.colleges.entity.Types;
import com.system.colleges.mapper.TypesMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypesService {

    @Autowired
    TypesMapper mapper;

    public List<Types> queryAllTypes(){
        return mapper.queryAllTypes();
    }

    public List<Types> queryAllByBig(String type){
        return mapper.queryAllByBig(type);
    }

}
