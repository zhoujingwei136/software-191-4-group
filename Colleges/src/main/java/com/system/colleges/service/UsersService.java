package com.system.colleges.service;

import com.system.colleges.entity.Colleges;
import com.system.colleges.entity.Users;
import com.system.colleges.mapper.UsersMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersService {

    @Autowired
    UsersMapper mapper;

    public List<Users> queryAllUsers(){
        return mapper.queryAllUsers();
    }

    public Users findUsersById(String id){
        return mapper.findUsersById(id);
    }

    public void Register(Users users){
        mapper.addUsers(users);
    }

    public Users login(Users user){
        return mapper.findUsersByName(user.getName());
    }

    public int findUserByName(String name){
        Users u = mapper.findUsersByName(name);
        if (u!=null){   //已经有人占用了
            return 0;
        }else return 1;
    }

    public int findUsersByNameWhileReg(String name){
        Users u = mapper.findUsersByNameWhileReg(name);
        if (u!=null){   //已经有人占用了
            return 0;
        }else return 1;
    }

    public Users loginById(Users users){
        return mapper.findUsersById(users.getId());
    }

    public int getRanks(int score){
        int ranks;
        if(score>700){
            ranks = 1;
        }else if(score >680){
            ranks = 24;
        }else if(score >650){
            ranks = 1695;
        }else if(score >600){
            ranks = 16931;
        }else if(score >575){
            ranks = 34285;
        }else if(score >550){
            ranks = 61420;
        }else if(score >545){
            ranks = 68091;
        }else if(score >540){
            ranks = 75364;
        }else if(score >535){
            ranks = 83238;
        }else if(score >530){
            ranks = 91575;
        }else if(score >525){
            ranks = 100531;
        }else if(score >520){
            ranks = 109774;
        }else if(score >515){
            ranks = 119504;
        }else if(score >510){
            ranks = 129715;
        }else if(score >505){
            ranks = 140178;
        }else if(score >500){
            ranks = 151107;
        }else if(score >475){
            ranks = 208438;
        }else if(score >450){
            ranks = 264228;
        }else if(score >425){
            ranks = 313269;
        }else if(score >400){
            ranks = 355287;
        }else if(score >300){
            ranks = 489245;
        }else if(score >200){
            ranks = 541715;
        }else if(score >100){
            ranks = 544424;
        }else if(score>50){
            ranks = 699424;
        }else {
            ranks = 756521;
        }
        return ranks;
    }

    public String getTypeString(String type){
        char[] chars = type.toCharArray();
        String[] strings = new String[chars.length];
        for(int i=0;i<chars.length;i++){
            char c = chars[i];
            if(c=='1') strings[i]="物理";
            if(c=='2') strings[i]="化学";
            if(c=='3') strings[i]="生物";
            if(c=='4') strings[i]="政治";
            if(c=='5') strings[i]="历史";
            if(c=='6') strings[i]="地理";
        }
        String realType = "";
        for(int i=0;i<strings.length;i++){
            if(i!=strings.length-1)
                realType = realType+strings[i]+",";
            else realType = realType+strings[i];
        }
        return realType;
    }

    public void updateUsers(Users users){
        mapper.updateUsers(users);
    }

    public int getUsersRoleById(String id){
        if(mapper.getUsersRoleById(id)!=0)
            return 1;
        else return 0;
    }

    public List<Users> findUsersByPage(Integer page, Integer rows){
        int start = (page-1)*rows;
        return mapper.findUsersByPage(start,rows);
    }

    public void deleteUsers(String id){
        mapper.deleteUsers(id);
    }

}
