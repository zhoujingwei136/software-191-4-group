package com.system.colleges.service;

import com.system.colleges.entity.CollegeMajors;
import com.system.colleges.mapper.CollegeMajorsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollegeMajorsService {

    @Autowired
    CollegeMajorsMapper mapper;

    public List<CollegeMajors> queryAllMajorsByCollege(String cid){
        return mapper.queryAllMajorsByCollege(cid);
    }

    public CollegeMajors queryCollegeMajorById(String id){
        return mapper.queryCollegeMajorById(id);
    }

    public List<CollegeMajors> queryEmphasisedByCid(String cid){
        return mapper.queryEmphasisedByCid(cid);
    }

    public String getRealType(CollegeMajors cm){
        String s0 = cm.getType();
        char[] chars = s0.toCharArray();
        String[] strings = new String[chars.length];
        for(int i=0;i<chars.length;i++){
            char c = chars[i];
            if(c=='1') strings[i]="物理";
            if(c=='2') strings[i]="化学";
            if(c=='3') strings[i]="生物";
            if(c=='4') strings[i]="政治";
            if(c=='5') strings[i]="历史";
            if(c=='6') strings[i]="地理";
        }
        String realType = "";
        for(int i=0;i<strings.length;i++){
            if(i!=strings.length-1)
                realType = realType+strings[i]+",";
            else realType = realType+strings[i];
        }
        return realType;
    }

    public String getOneRealType(char c){
        String str = "";
        if(c=='1') str="物理";
        if(c=='2') str="化学";
        if(c=='3') str="生物";
        if(c=='4') str="政治";
        if(c=='5') str="历史";
        if(c=='6') str="地理";
        return str;
    }

    public List<CollegeMajors> queryAllCollegeMajors(Integer lowest){
        return mapper.queryAllCollegeMajors(lowest);
    }

}
