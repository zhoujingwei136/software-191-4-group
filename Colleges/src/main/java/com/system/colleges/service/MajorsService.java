package com.system.colleges.service;

import com.system.colleges.entity.Majors;
import com.system.colleges.mapper.MajorsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MajorsService {

    @Autowired
    MajorsMapper mapper;

    public Majors queryMajorById(String id){
        return mapper.queryMajorById(id);
    }

    public List<Majors> queryMajorByType(String type){
        return mapper.queryMajorByType(type);
    }

    public Majors queryMajorByName(String name){
        return mapper.queryMajorByName(name);
    }

    public List<Majors> queryAllMajors(){
        return mapper.queryAllMajors();
    }

    public void deleteMajors(String id){
        mapper.deleteMajors(id);
    }

}
