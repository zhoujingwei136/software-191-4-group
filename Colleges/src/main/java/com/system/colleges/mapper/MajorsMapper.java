package com.system.colleges.mapper;

import com.system.colleges.entity.Majors;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface MajorsMapper {

    Majors queryMajorById(String id);

    List<Majors> queryMajorByType(String type);

    Majors queryMajorByName(String name);

    List<Majors> queryAllMajors();

    void deleteMajors(String id);

}
