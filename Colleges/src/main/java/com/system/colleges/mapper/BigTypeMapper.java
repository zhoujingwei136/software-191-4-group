package com.system.colleges.mapper;

import com.system.colleges.entity.BigType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface BigTypeMapper {

    @Select("select * from college.bigtype")
    List<BigType> queryAll();

}
