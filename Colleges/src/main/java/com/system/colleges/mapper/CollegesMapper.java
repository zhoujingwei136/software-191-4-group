package com.system.colleges.mapper;

import com.system.colleges.entity.Colleges;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CollegesMapper {

    List<Colleges> queryAllColleges();

    Colleges queryCollegeById(String id);

    List<Colleges> findCollegesByPage(@Param("start") Integer start, @Param("rows") Integer rows);

    Integer findTotals();

    List<Colleges> findCollegesByPageAndType(@Param("start") Integer start, @Param("rows") Integer rows, @Param("type")String type);
    Integer findTotalsByType(String type);

    List<Colleges> findCollegesByPageAndRegion(@Param("start") Integer start, @Param("rows") Integer rows, @Param("region")String region);
    Integer findTotalsByRegion(String region);

    List<Colleges> findCollegesByTwo
            (@Param("start") Integer start, @Param("rows") Integer rows, @Param("type")String type, @Param("region")String region);
    Integer findTotalsByTwo(@Param("type")String type, @Param("region")String region);

    List<String> queryAllCollegesName();

    void deleteCollege(String id);

    void updateCollege(Colleges colleges);

    void addCollege(Colleges colleges);

}
