package com.system.colleges.mapper;

import com.system.colleges.entity.Types;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TypesMapper {

    @Select("select * from college.types")
    List<Types> queryAllTypes();

    @Select("select * from college.types where type=#{type}")
    List<Types> queryAllByBig(String type);

}
