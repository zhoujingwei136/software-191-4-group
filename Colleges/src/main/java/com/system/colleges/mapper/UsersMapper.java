package com.system.colleges.mapper;

import com.system.colleges.entity.Colleges;
import com.system.colleges.entity.Users;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UsersMapper {

    List<Users> queryAllUsers();

    void addUsers(Users users);

    Users findUsersByNameWhileReg(String name);

    Users findUsersByName(String name);

    Users findUsersById(String id);

    void updateUsers(Users users);

    int getUsersRoleById(String id);

    List<Users> findUsersByPage(@Param("start") Integer start, @Param("rows") Integer rows);

    void deleteUsers(String id);

}
