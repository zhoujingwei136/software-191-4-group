package com.system.colleges.mapper;

import com.system.colleges.entity.CollegeMajors;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CollegeMajorsMapper {

    List<CollegeMajors> queryAllMajorsByCollege(String cid);

    CollegeMajors queryCollegeMajorById(String id);

    List<CollegeMajors> queryEmphasisedByCid(String cid);

    List<CollegeMajors> queryAllCollegeMajors(Integer lowest);

}
