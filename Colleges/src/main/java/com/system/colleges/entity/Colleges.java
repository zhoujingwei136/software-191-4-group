package com.system.colleges.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
//@ToString
@Accessors(chain=true)
public class Colleges {

    private int id;
    private String name;
    private String describes;
    private String logo;
    private String region;
    private String type;
    private String level;
    private int lowest;
    private String teachers;
    private String website;

    @Override
    public String toString() {
        return "Colleges{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", describes='" + describes.substring(0,10) + '\'' +
                ", logo='" + logo + '\'' +
                ", region='" + region + '\'' +
                ", type='" + type + '\'' +
                ", level='" + level + '\'' +
                ", lowest=" + lowest +
                ", teachers='" + teachers.substring(0,10) + '\'' +
                ", website='" + website + '\'' +
                '}';
    }
}
