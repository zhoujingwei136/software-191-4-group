package com.system.colleges.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain=true)
public class CollegeMajors {

    private int id;
    private int cid;
    private int mid;
    private String mname;
    private String type;
    private int lowest;
    private int average;
    private int number;
    private int emphasised;

}
