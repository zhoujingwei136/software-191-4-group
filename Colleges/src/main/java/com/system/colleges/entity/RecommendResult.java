package com.system.colleges.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain=true)
public class RecommendResult {

    public CollegeMajors collegeMajors;
    public Integer weight;
    public Colleges college;
    public String Type;

}
