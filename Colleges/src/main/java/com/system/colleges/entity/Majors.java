package com.system.colleges.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain=true)
public class Majors {

    private int id;
    private String name;
    private String describes;
    private String type;
    private String level;
    private String goal;
    private String courses;
    private String ability;
    private String needed;

}
