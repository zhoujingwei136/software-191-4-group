package com.system.colleges.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CollegeMajorWeight {

    public CollegeMajors collegeMajors;
    public Integer weight;

}
