/*
SQLyog Enterprise v12.09 (64 bit)
MySQL - 8.0.23 : Database - college
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`college` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `college`;

/*Table structure for table `bigtype` */

DROP TABLE IF EXISTS `bigtype`;

CREATE TABLE `bigtype` (
  `id` int NOT NULL AUTO_INCREMENT,
  `NAME` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `bigtype` */

insert  into `bigtype`(`id`,`NAME`) values (1,'哲学'),(2,'哲学'),(3,'理学'),(4,'工学');

/*Table structure for table `college_majors` */

DROP TABLE IF EXISTS `college_majors`;

CREATE TABLE `college_majors` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cid` int NOT NULL,
  `mname` varchar(32) NOT NULL,
  `TYPE` varchar(256) NOT NULL,
  `lowest` int NOT NULL,
  `average` int DEFAULT NULL,
  `number` int DEFAULT NULL,
  `emphasised` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `college_majors` */

insert  into `college_majors`(`id`,`cid`,`mname`,`TYPE`,`lowest`,`average`,`number`,`emphasised`) values (1,1,'建筑学','物理,历史,地理',577,583,49,1),(2,1,'风景园林','物理,生物,地理',551,557,34,1),(3,1,'城乡规划','物理,历史,地理',550,559,61,1),(4,1,'工程造价','物理',545,550,54,0),(5,1,'会计学','不限',540,546,68,0),(6,1,'英语','不限',539,542,50,0),(7,1,'土木工程','物理',538,544,221,1),(8,1,'电气工程及其自动化','物理',537,542,97,0),(9,1,'法学','政治',536,543,102,0),(10,1,'风景园林','物理,生物,地理',535,541,68,0),(11,1,'计算机类','物理',528,534,400,0),(12,1,'建筑电气与智能化','物理',529,536,26,0),(13,1,'电子信息工程','物理',526,529,64,0),(14,1,'城市地下空间工程','物理',528,534,49,0),(15,1,'工商管理','不限',530,534,30,0),(16,1,'应用统计学','物理',527,533,14,0),(17,2,'计算机科学与技术','物理',685,687,5,1),(18,2,'软件工程','物理',684,685,2,1),(19,2,'网络工程','物理',681,685,3,0),(20,2,'测绘地理工程','物理,地理',682,682,1,0),(21,2,'地质学','物理,地理',682,682,1,0),(22,2,'土木工程','物理',679,679,38,0),(23,2,'建筑学','物理',683,685,35,1),(24,2,'临床医学','物理,化学,生物',695,695,1,0),(25,2,'临床医学(协和)','物理,化学,生物',695,695,1,0),(26,2,'金融学','不限',706,706,1,0),(27,2,'机械工程','物理',679,685,22,1),(28,2,'自动化','物理',681,685,30,1),(29,2,'理论化学','化学',680,685,15,0),(30,2,'物理学','物理',686,689,10,1),(31,3,'社会学类','不限',687,687,1,0),(32,3,'国际政治','不限',687,687,2,0),(33,3,'新闻传播学','不限',687,687,1,0),(34,3,'公共管理类','不限',687,687,1,0),(35,3,'中国语言文学类','不限',688,689,2,0),(36,3,'哲学类','不限',688,688,1,0),(37,3,'信息管理与信息系统','不限',688,688,1,0),(38,3,'城乡规划','不限',688,688,1,0),(39,3,'化学类','物理,化学',690,692,2,0),(40,3,'环境科学与工程类','物理,化学',690,691,2,0),(41,3,'电子信息类','物理',693,695,5,1),(42,3,'计算机类','物理',693,696,10,1),(43,3,'数学','物理',695,700,5,1),(44,3,'物理学','物理',695,700,5,1),(45,3,'金融学','不限',697,698,4,1),(46,5,'人文科学','历史,地理',673,673,3,0),(47,5,'社会科学','不限',675,677,22,0),(48,5,'信息与计算科学','物理',679,680,2,0),(49,5,'数学与应用数学','物理',679,680,1,1),(50,5,'电气与自动化','物理',680,681,3,0),(51,5,'电子信息工程','物理',680,681,3,0),(52,5,'新闻学','不限',680,681,1,0),(53,5,'金融学','不限',680,681,5,0),(54,5,'临床医学','物理,化学',683,685,4,0),(55,5,'机器人工程','物理',684,684,2,1),(56,5,'机械与材料','物理',685,685,1,0),(57,5,'机械工程','物理',685,685,1,0),(58,5,'人工智能','物理',686,687,1,1),(59,5,'计算机科学与技术','物理',685,687,2,0),(60,6,'行政管理','不限',656,657,1,0),(61,6,'公共事业管理','不限',656,657,1,0),(62,6,'智慧建筑规划','物理,历史,地理',657,658,2,1),(63,6,'信息管理与信息系统','物理',657,658,3,0),(64,6,'物流管理','物理',657,658,4,0),(65,6,'机械设计制造及其自动化','物理',659,661,20,0),(66,6,'先进制造','物理',659,661,11,0),(67,6,'工商管理','不限',659,660,2,0),(68,6,'会计学','不限',659,660,2,0),(69,6,'材料成型及控制工程','物理',659,659,2,1),(70,6,'经济学','物理,化学,生物',659,660,2,0),(71,6,'物理学','物理',662,663,1,0),(72,6,'应用物理学','物理',662,663,3,1),(73,7,'英语','不限',673,676,1,0),(74,7,'法语','不限',673,676,1,0),(75,7,'汉语言文学','不限',673,676,5,0),(76,7,'社会学','不限',674,675,1,0),(77,7,'国际政治','不限',674,675,2,0),(78,7,'行政管理','不限',674,675,1,0),(79,7,'经济学','不限',675,676,2,0),(80,7,'金融学','不限',675,676,2,0),(81,7,'电子商务','不限',675,676,1,0),(82,7,'国际经济与贸易','不限',675,676,3,0),(83,7,'环境科学','物理,化学',676,677,2,1),(84,7,'环境工程','物理,化学',676,677,2,0),(85,7,'物理学','物理',677,678,10,0),(86,7,'天文学','物理',677,678,3,1),(87,8,'材料科学与工程','物理',631,634,35,0),(88,8,'材料成型及控制工程','物理',631,634,30,0),(89,8,'工程力学','物理',632,635,10,0),(90,8,'土木工程','物理',632,635,7,0),(91,8,'水利与海洋工程','物理',632,635,3,1),(92,8,'哲学','不限',633,636,12,0),(93,8,'环境工程','物理,化学,生物',633,636,10,1),(94,8,'环境科学','物理,化学,生物',633,636,20,0),(95,8,'社会学','不限',634,636,10,0),(96,8,'社会工作','不限',634,636,5,0),(97,8,'英语','不限',634,637,40,0),(98,8,'西班牙语','不限',634,637,20,0),(99,8,'俄语','不限',634,637,10,0),(100,8,'机械设计制造及其自动化','物理',634,636,50,1),(101,9,'地质学','物理,化学',605,606,5,1),(102,9,'地址勘查工程','物理,化学',605,606,2,1),(103,9,'安全工程','物理',606,608,5,0),(104,9,'海洋汽油工程','物理,化学',606,606,6,1),(105,9,'环境工程','物理,化学,生物',607,609,2,0),(106,9,'环境科学','物理,化学,生物',607,609,4,0),(107,9,'机器人工程','物理',607,610,21,1),(108,9,'石油工程','物理,化学',607,611,9,0),(109,9,'材料科学与工程','物理',608,610,6,1),(110,9,'化工与制药','物理,化学,生物',608,610,7,0),(111,9,'新能源科学与工程','物理,化学',611,613,4,0),(112,9,'应用化学','物理,化学',611,612,4,0),(113,10,'材料化学','化学',522,528,140,0),(114,10,'复合材料与工程','化学',522,528,106,0),(115,10,'功能材料','化学',523,528,53,0),(116,10,'印刷工程','物理,化学',525,532,46,0),(117,10,'包装工程','物理,化学',525,532,20,0),(118,10,'环境科学','物理,化学,生物',526,531,138,1),(119,10,'化工与制药','化学',526,532,136,0),(120,10,'轻化工程','化学',526,533,89,1),(121,10,'葡萄与葡萄酒工程','物理,化学,生物',527,531,27,0),(122,10,'应用化学','化学',528,535,99,0),(123,10,'测控技术与仪器','物理',530,533,48,0),(124,10,'生物技术','物理,化学,生物',530,536,57,0),(125,4,'人工智能','物理',685,687,8,1),(126,4,'自动化','物理',686,688,1,1),(127,4,'信息工程','物理',686,688,2,0),(128,4,'计算机科学与技术','物理',686,688,1,0),(129,4,'信息安全','物理',686,688,3,1),(130,4,'金融学','物理',685,687,1,0),(131,4,'法学','不限',684,686,2,0),(132,4,'动物科学','物理,化学',685,688,1,0),(133,4,'食品科学与工程','物理,化学',685,688,1,0),(134,4,'工商管理','物理',685,687,2,0),(135,4,'海洋工程','物理',686,688,1,1),(136,4,'电子信息','物理',687,688,1,0),(137,4,'应用化学','物理,化学',686,688,1,0);

/*Table structure for table `colleges` */

DROP TABLE IF EXISTS `colleges`;

CREATE TABLE `colleges` (
  `id` int NOT NULL AUTO_INCREMENT,
  `NAME` varchar(32) NOT NULL,
  `describes` text,
  `logo` text,
  `region` varchar(32) DEFAULT NULL,
  `TYPE` varchar(32) DEFAULT NULL,
  `LEVEL` varchar(32) DEFAULT NULL,
  `lowest` int DEFAULT NULL,
  `teachers` text,
  `website` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `colleges` */

insert  into `colleges`(`id`,`NAME`,`describes`,`logo`,`region`,`TYPE`,`LEVEL`,`lowest`,`teachers`,`website`) values (1,'山东建筑大学','山东建筑大学是一所以建筑土木类专业为特色，并兼有文、管等学科专业门类省属高等院校。它始建于1956年，前身是山东建筑工程学院。学校座落于山东省济南市历城临港开发区。学校是国家“卓越工程师教育培养计划”高校，是首批山东特色名校工程单位之一。 截至2013年12月底，学校有新校区、和平校区两个校区，占地面积2700余亩，校舍面积70余万平方米，教学科研仪器设备总值2.2亿元。学校设有16个学院，拥有57个本科专业，有教职工1997人，其中专任教师1323人，具有高级职称的教师666人，兼职博士生导师26人，硕士生导师314人，有全日制在校生2.6万人。山东建筑大学始建于1956年。学校秉承“厚德博学、筑基建业”的校训，弘扬“勤奋、严谨、团结、创新”的校风，强化内涵建设，凝练办学特色，已发展成为一所以工科为主，以土木建筑为特色，工学、理学、管理学、文学、法学、农学、艺术学等交叉渗透、协调发展的多科性大学。学校是国务院批准首批学士学位授权单位、首批山东特色名校工程（省版211）、卓越工程师教育培养计划、全国建筑类高校就业联盟（ACEA）成员、服务国家特殊需求“绿色建筑技术及其理论”博士人才培养高校、山东省高校协同创新中心首批立项建设单位。','sdjz.jpg','山东','工科','普通本科',504,'学校现有教职工1997人，其中专任教师1323人，具有高级职称的教师666人，兼职博士生导师26人，硕士生导师314人。有双聘院士3人，国家新世纪人才工程人员5人，享受国务院特殊津贴专家16人，国家级教学名师1人，全国优秀教师13人，全职“泰山学者”特聘教授1人，泰山学者特聘教授及海外特聘专家12人，山东省有突出贡献的中青年专家17人，山东省级教学名师9人，山东省优秀教师13人。','https://www.sdjzu.edu.cn/'),(2,'清华大学','清华大学（TsinghuaUniversity）简称清华，诞生于1911年，因北京西北郊清华园得名，依托美国退还的部分“庚子赔款”建立，初称“清华学堂”，是清政府设立的留美预备学校，翌年更名为“清华学校”；为尝试人才的本地培养，1925年设立大学部，1928年更名为“国立清华大学”。1937年抗日战争爆发后，学校南迁长沙，与北京大学、南开大学联合组建国立长沙临时大学，1938年迁至昆明，改名为“国立西南联合大学”；1946年，迁回清华园原址复校。 学校由中华人民共和国教育部直属，位列“211工程”、“985工程”，入选“珠峰计划”、“2011计划”、“111计划”、“卓越工程师教育培养计划”、“卓越法律人才教育培养计划”、“卓越医生教育培养计划”，是C9联盟、东亚研究型大学协会、环太平洋大学联盟、清华—剑桥—麻省理工学院低碳能源大学联盟成员，中管副部级建制。 清华大学是中国乃至亚洲最著名的高等学府之一，在长达百年的历史进程中，2位共和国主席、7位中央政治局常委、14名两弹一星获得者、600余名院士从这里走出，王国维、梁启超、陈寅恪、赵元任等一大批学术巨匠曾在此执教，恰如清华园工字殿内对联所书——“槛外山光，历春夏秋冬、万千变幻，都非凡境；窗中云影，任东西南北、去来澹荡，洵是仙居”，百年清华必将肩负中华民族伟大复兴的重要使命，为社会繁荣、国家富强、人类进步作出更为广阔、深远、历久弥新的璀璨贡献。','qh.jpg','北京','综合','985 211 双一流',681,'清华大学培育和凝聚了一批又一批高水平的专家学者。近年来，学校遵循\"引进与培养并举\"的方针，加强青年教师队伍建设，在国内外选聘优秀人才，师资队伍水平稳步提高。 学校现有教师3461人，其中45岁以下青年教师1758人。教师中具有正高级职务的1400人，具有副高级职务的1582人。现有教师中有诺贝尔奖获得者1名，图灵奖获得者1名，中国科学院院士51名，中国工程院院士37名，16名教授荣获国家级\"高等学校教学名师奖\"，163人入选教育部\"长江学者奖励计划\"特聘教授，58人入选讲座教授，36人入选青年学者，221人获得\"国家杰出青年科学基金\"，133人获得\"优秀青年科学基金\"，\"海外高层次人才引进计划\"（简称\"千人计划\"）入选者118人，在职\"青年千人计划\"入选者137人。','https://www.sdjzu.edu.cn/'),(3,'北京大学','北京大学（PekingUniversity），简称北大，创建于1898年，初名京师大学堂，是中国近代第一所国立综合性大学，也是当时中国最高教育行政机关。1911年辛亥革命爆发，翌年改为现名；1917年，著名教育家蔡元培出任北京大学校长，“循思想自由原则、取兼容并包之义”，推行改革，把北大办成以文、理两科为重点的综合性大学，使北京大学成为新文化运动的中心、五四运动的策源地。1937年抗日战争爆发，北大与清华、南开合并组建国立西南联合大学；1946年，北大迁回北平复校。 1998年5月4日，北京大学举办百年校庆，在国家的支持下，北京大学适时启动了“创建世界一流大学计划”。2000年4月3日，北京大学与原北京医科大学合并，组建新的北京大学。学校为教育部直属全国重点大学，国家“211工程”、“985工程”建设大学、C9联盟，以及东亚研究型大学协会、国际研究型大学联盟、环太平洋大学联盟、东亚四大学论坛的重要成员。 巍巍上庠，国运所系。一百多年来，北京大学的发展与国家和民族的命运息息相关，形成了光荣的革命传统和优良的学术传统。作为中国的最高学府，北京大学聚集了各个历史时期的众多著名学者、专家，培养了一代又一代优秀人才，创造了一批又一批重大科学成果，深刻影响了中国近现代思想理论、科学技术、文化教育和社会发展的进程。','bj.jpg','北京','综合','985 211 双一流',687,'截至2017年12月，北大拥有教职工（不包含博士后）21183人；专任教师数7317人，其中，按职称划分：正高级2217人，副高级2231人；其中：中国科学院院士76人，中国工程院院士19人，发展中国家科学院院士25人，哲学社会科学资深教授13人，“千人计划”入选者72人，“青年千人计划”入选者153人，“万人计划”入选者28人，“青年拔尖人才计划”入选者35人，“长江学者奖励计划”特聘教授、讲座教授、青年学者231人，国家杰出青年基金获得者237人，国家基金委创新群体40个，国家基金委优秀青年基金获得者130人，国家级教学名师17人，博士生导师2474人，科研机构人员1161人，附属医院教职工10131人。','https://www.sdjzu.edu.cn/'),(4,'上海交通大学','上海交通大学是我国历史最悠久、享誉海内外的著名高等学府之一，是教育部直属并与上海市共建的全国重点大学。经过120多年的不懈努力，上海交通大学已经成为一所“综合性、研究型、国际化”的国内一流、国际知名大学。 学校现有徐汇、闵行、黄浦、长宁、七宝、浦东等校区，总占地面积300余万平方米。通过一系列的改革和建设，学校的各项办学指标大幅度上升，实现了跨越式发展，整体实力显著增强，为建设世界一流大学奠定了坚实的基础。 交通大学始终把人才培养作为办学的根本任务。一百多年来，学校为国家和社会培养了逾30万各类优秀人才，包括一批杰出的政治家、科学家、社会活动家、实业家、工程技术专家和医学专家，如江泽民、陆定一、丁关根、汪道涵、钱学森、吴文俊、徐光宪、张光斗、黄炎培、邵力子、李叔同、蔡锷、邹韬奋、严隽琪、陈敏章、王振义、陈竺等。在中国科学院、中国工程院院士中，有200余位交大校友；在国家23位“两弹一星”功臣中，有6位交大校友；在国家最高科学技术奖获得者中，有3位来自交大。 截至2018年12月，学校共有30个学院/直属系，31个研究院，13家附属医院，2个附属医学研究所，12个直属单位，6个直属企业。全日制本科生（国内）16129人、研究生（国内）30217人（其中全日制硕士研究生14439人、全日制博士研究生7882人），学位留学生2982人，其中研究生学位留学生1698人；有专任教师3061名，其中教授982名。 学校现有本科专业67个，涵盖经济学、法学、文学、理学、工学、农学、医学、管理学和艺术等九个学科门类；新世纪以来获49项高等教育国家级教学成果奖（其中36项为第一完成单位）。','shjt.jpg','上海','综合','985 211 双一流',685,'学校现有专任教师3061名，其中教授982名；中国科学院院士22名、中国工程院院士22名（包括1名两院院士），“长江学者”特聘教授和讲座教授共144名，国家杰出青年基金获得者144名，青年拔尖人才24名，长江青年学者28名，优秀青年科学基金获得者86名，国家重点基础研究发展计划（973计划）首席科学家36名（青年科学家2名），国家重大科学研究计划首席科学家14名，国家基金委创新研究群体16个，教育部创新团队20个，国家重点研发计划项目获得者73名（青年项目获得者7名）。 现今学校有国家“万人计划”教学名师2人，国家高层次人才特殊支持计划1人，国家级高等学校教学名师奖获得者8人，上海市教学名师奖获得者33人，国家级教学团队8个，上海市教学团队15个；有国家级视频公开课、精品资源共享课程、精品在线开放课程、双语示范课程等48门，上海市精品课程、重点课程、高校示范性全英语课程等343门。','https://www.sdjzu.edu.cn/'),(5,'浙江大学','浙江大学是一所历史悠久、声誉卓著的高等学府，坐落于中国历史文化名城、风景旅游胜地杭州。习近平总书记主政浙江期间，亲自联系浙江大学，18次莅临指导，对学校改革发展作出了一系列重要指示，描绘了高水平建成中国特色世界一流大学的宏伟蓝图。在120多年的办学历程中，浙江大学始终秉承以“求是创新”为校训的优良传统，以天下为己任、以真理为依归，逐步形成了“勤学、修德、明辨、笃实”的浙大人共同价值观和“海纳江河、启真厚德、开物前民、树我邦国”的浙大精神。 浙江大学是一所特色鲜明、在海内外有较大影响的综合型、研究型、创新型大学，其学科涵盖哲学、经济学、法学、教育学、文学、历史学、艺术学、理学、工学、农学、医学、管理学等十二个门类。设有7个学部、37个专业学院（系）、1个工程师学院、2个中外合作办学学院、7家附属医院。学校现有紫金港、玉泉、西溪、华家池、之江、舟山、海宁等7个校区，占地面积5870761平方米，校舍总建筑面积2746377平方米，图书馆总藏书量777.8万册。截至2018年底，浙江大学现有全日制在校学生54641人（其中：本科生25425人，硕士研究生19038人，博士研究生10178人），国际学生7074人（其中：攻读学位的国际学生4409人）。有教职工8909人（其中：专任教师3741人），教师中有中国科学院院士、中国工程院院士（含双聘）45人、文科资深教授10人、教育部“长江学者奖励计划”特聘（讲座）教授121人、国家杰出青年科学基金获得者133人。在国家公布的“双一流”建设名单中，学校入选一流大学建设高校（A类），18个学科入选一流建设学科，居全国高校第三。根据2019年5月ESI公布的数据，学校有8个学科进入世界学术机构排名前1‰，居全国高校第一；有1个学科进入世界学术机构排名前万分之一，有8个学科进入世界学术机构排名前100位，均居全国高校第二；有5个学科进入世界学术机构排名前50位，居全国高校第三。 浙江大学紧紧围绕“德才兼备、全面发展”的核心要求，全面落实立德树人根本任务，着力培养德智体美劳全面发展、具有全球竞争力的高素质创新人才和领导者。在长期的办学历程中，涌现出大批著名科学家、文化大师以及各行各业的精英翘楚，校友中当选为两院院士的有200余人。学校始终保持与时俱进的教育思想，教育教学模式改革走在了全国高校前列；丰富的校园文化、先进的教学设施和广泛的国际交流为学生成长创造了优越条件。2018届毕业生初次就业率达到97.64%，本科毕业生海内外深造率达到60.30%。','zj.jpg','浙江','综合','985 211 双一流',673,'学校现有教职工8909人（其中：专任教师3741人），教师中有中国科学院院士、中国工程院院士（含双聘）45人、文科资深教授10人、教育部“长江学者奖励计划”特聘（讲座）教授121人、国家杰出青年科学基金获得者133人。在国家公布的“双一流”建设名单中，学校入选一流大学建设高校（A类），18个学科入选一流建设学科，居全国高校第三。根据2019年5月ESI公布的数据，学校有8个学科进入世界学术机构排名前1‰，居全国高校第一；有1个学科进入世界学术机构排名前万分之一，有8个学科进入世界学术机构排名前100位，均居全国高校第二；有5个学科进入世界学术机构排名前50位，居全国高校第三。','https://www.sdjzu.edu.cn/'),(6,'华中科技大学','华中科技大学（HuazhongUniversityofScienceandTechnology）是中华人民共和国教育部直属的一所综合性全国重点大学，是国家“211工程”、“985工程”首批重点建设高校，是“卓越工程师教育培养计划”、“卓越医生教育培养计划”、“2011计划”、“111计划”、“千人计划”入选高校，是21世纪学术联盟、中俄工科大学联盟、七校联合办学成员，是与国家卫生和计划生育委员会共建医学院的十所院校之一，是拥有国家实验室和国家大科学中心的四所大学之一，是被美国制造工程师协会（SME）授予“大学领先奖”的两所中国大学之一，入选《Nature》评出的“中国十大科研机构”，被称作“新中国高等教育发展的缩影”。 学校坐落于湖北省武汉市喻家山麓、东湖之畔，被誉为“森林大学”。学校前身分别为1952年创建的华中理工大学、1907年创建的同济医科大学以及发源于1898年的武汉城市建设学院，三校在2000年5月26日合并组建为全新的华中科技大学。 截止2014年4月，学校有本科生32449人，研究生22837人，其中博士研究生6445人、硕士研究生16392人，有各类留学生1745人。','hzkj.jpg','湖北','综合','985 211 双一流',656,'学校实施“人才兴校”战略，师资力量雄厚，始终坚持引进和培养并举，以保证数量和规模为前提，以中青年教师培养为基础，以高层次人才建设为重点，突出办学特色，强化内涵发展，不断提高师资队伍整体素质和创新能力。。现有专任教师3400余人，其中教授1200余人，副教授1400余人；教师中有院士17人，长江学者特聘教授59人、长江学者青年项目15人，国家杰出青年科学基金获得者69人，“973计划”项目首席科学家15人，重大科学研究计划项目首席科学家2人，国家重点研发计划项目首席科学家24人，973计划（含重大科学研究计划）青年科学家3人，优秀青年科学基金获得者49人，国家级教学名师9人，“万人计划”领军人才29人、青年拔尖人才21人，教育部新世纪优秀人才支持计划入选者224人，国家百千万人才工程入选者40人。','https://www.sdjzu.edu.cn/'),(7,'南京大学','南京大学坐落于钟灵毓秀、虎踞龙蟠的金陵古都，是一所历史悠久、声誉卓著的百年名校。 在一个多世纪的办学历程中，南京大学及其前身与时代同呼吸、与民族共命运，谋国家之强盛、求科学之进步，为国家的富强和民族的振兴做出了重要的贡献。尤其是改革开放以来，作为教育部直属的重点综合性大学，南京大学又在崭新的历史机遇中焕发出新的生机，在教学、科研和社会服务等各个领域保持良好的发展态势，各项办学指标和综合实力均位居全国高校前列。1994年，南京大学被确定为国家“211工程”重点支持的大学；1999年，南京大学进入国家“985工程”首批重点建设的高水平大学行列；2006年，教育部和江苏省再次签订重点共建南京大学的协议；2011年，教育部和江苏省签署协议继续重点共建南京大学；2016年，南京大学入选首批国家级双创示范基地；2017年，南京大学入选A类世界一流大学建设高校名单，15个学科入选世界一流学科建设名单。 南京大学目前拥有仙林、鼓楼、浦口、苏州四个校区，有31个直属院系，各类学生总计56068人，其中本科生13243人、硕士研究生13406人、博士研究生6496人、外国留学生3378人。全校有一级学科国家重点学科8个，二级学科国家重点学科13个，“双一流”建设学科15个，江苏高校优势学科建设工程三期项目立项学科19个，江苏省一级学科重点学科18个，本科专业88个，博士学位授权一级学科38个，博士学位授权二级学科点（不含一级学科覆盖点）3个，专业博士学位授权点1个，硕士学位授权一级学科8个，硕士学位授权二级学科点（不含一级学科覆盖点）6个，专业硕士学位授权点25个，博士后流动站38个，国家基础学科人才培养基地12个，国家生命科学与技术人才培养基地1个。','nj.jpg','江苏','综合','985 211 双一流',673,'南京大学拥有一支高素质的师资队伍，其中包括中国科学院院士28人，中国工程院院士3人，中国科学院外籍院士1人，在国际或他国当选院士13人（含第三世界科学院院士4人，发展中国家科学院院士1人，俄罗斯科学院院士1人，加拿大皇家科学院院士1人，欧洲文理科学院院士1人，欧洲科学院外籍院士2人，国际量子分子科学院院士1人，俄罗斯艺术科学院荣誉院士1人，法兰西艺术院通讯院士1人），教育部“长江学者奖励计划”特聘教授100人、讲座教授25人、“青年长江学者”14人，国家杰出青年基金获得125人、优秀青年科学基金项目获得者70人，国家科技重大专项、“973计划”、“863计划”、国家重点研发计划等重大项目首席科学家89人次，国务院学位委员会学科评议组成员22人，“万人计划”科技创新领军人才22人、哲学社会科学领军人才6人、百千万工程领军人才2人、教学名师4人、青年拔尖人才20人，国家级教学名师10人，教育部“百千万人才工程”国家级人选34人，“青年千人计划”入选者108人。','https://www.sdjzu.edu.cn/'),(8,'山东大学','山东大学是一所历史悠久、学科齐全、学术实力雄厚、办学特色鲜明，在国内外具有重要影响的教育部直属重点综合性大学，是世界一流大学建设高校（A类）。近年来山东大学实现了跨越式发展，各项事业均达到了前所未有的高度。学校的综合水平和办学质量明显提升，国际影响力显著增强，目前有16个学科的学术影响力和贡献能力进入ESI世界排名前1%，与30多个国家和地区的近170所学校签署了校际合作协议。学校规模宏大，实力雄厚。总占地面积8000余亩（含青岛校区约3000亩），形成了一校三地（济南、威海、青岛）的办学格局。现有4所附属医院，3所非隶属附属医院，11所教学、实习医院。拥有在职教职工7493人（不含附属医院）。各类全日制学生达6万人，其中，全日制本科生40789人，研究生18816人，留学生3791人。学校汇聚了一批杰出人才，共有教授1246人，博士生导师897人。 学校拥有精良的教学科研平台，有一级学科国家重点学科2个（涵盖8个二级学科）、二级学科国家重点学科14个、二级学科国家重点培育学科3个，省级重点学科70个，覆盖文、理、工、医四大学科领域，实现了各学科的协调发展。山东大学是中国目前学科门类最齐全的大学之一，在综合性大学中具有代表性。本科生和研究生层次教育涉及哲学、经济学、法学、教育学、文学、历史学、理学、工学、农学、医学、管理学、艺术学等12大学科门类。拥有一级学科博士学位授权点44个，一级学科硕士学位授权点55个，专业学位博士点3个，专业学位硕士点27个，本科专业117个，博士后科研流动站41个，形成了结构完整、实力雄厚、独具特色的人才培养体系。在历史发展中，山东大学形成了自己的学科优势和特色。','sd.jpg','山东','综合','985 211 双一流',631,'学校汇聚了一批杰出人才，共有教授1246人，博士生导师897人。其中，诺贝尔物理学奖获得者PeterGrünberg受聘为特聘教授，研究生导师莫言教授荣获2012年诺贝尔文学奖。学校现有中国科学院和工程院院士12人，“长江学者奖励计划”特聘教授37人、讲座教授15人、青年项目入选者5人；国家杰出青年科学基金获得者41人、优秀青年科学基金获得者29人；“万人计划”领军人才21人、教学名师5人、青年拔尖人才11人；国家百千万人才工程入选者32人；泰山学者攀登计划入选者15人、泰山学者特聘教授（专家）90人、泰山学者青年专家48人。','https://www.sdjzu.edu.cn/'),(9,'中国石油大学','中国石油大学（华东）是教育部直属全国重点大学，是国家“211工程”重点建设和开展“优势学科创新平台”建设并建有研究生院的高校之一。中国石油大学（华东）是教育部和五大能源企业集团公司、教育部和山东省人民政府共建的高校，是石油石化高层次人才培养的重要基地，被誉为“石油科技人才的摇篮”，现已成为一所以工为主、石油石化特色鲜明、多学科协调发展的大学。 学校现有青岛、东营两个校区，校园总面积4774亩，建筑面积122万平方米，图书馆藏书284万册。青岛校区地处迷人的帆船之都、海滨之城，享有极高美誉的青岛。东营校区地处黄河三角洲的中心城市、生态之城、石油之城——山东东营。两校区均位于“蓝黄”两大国家战略重点区域，青岛校区所在地同时也属于2014年新设立的国家级新区——青岛西海岸新区。学校建有研究生院，有地球科学与技术学院、石油工程学院、化学工程学院、机电工程学院、信息与控制工程学院、储运与建筑工程学院、计算机与通信工程学院、经济管理学院、理学院、文学院、马克思主义学院、体育教学部等12个教学学院（部），以及荟萃学院、国际教育学院、后备军官学院、远程教育学院和继续教育学院','zgsy.jpg','山东','工科','211 本科院校',599,'学校拥有一支师德高尚、业务精湛、结构合理、充满活力的高素质教师队伍。现有教师1700余人，其中教授、副教授1000余人，博士生导师222人。专任教师中有两院院士10人，国家“万人计划”入选者5人（领军人才3人、教学名师2人），“长江学者”特聘教授3人，国家杰出青年基金获得者5人；“长江学者”青年学者3人，国家优秀青年科学基金获得者2人；“973计划”项目首席科学家1人，国家“百千万人才工程”入选者11人，“新世纪优秀人才支持计划”入选者20人，中国青年科技奖获得者4人，教育部高校青年教师奖、霍英东教育基金会青年教师资助及青年教师奖获得者11人；“泰山学者”攀登计划2人，“泰山学者”特聘教授及海外特聘专家11人，“泰山学者”青年专家3人；“山东省有突出贡献的中青年专家”15人。国家级教学名师、省级教学名师15人，“全国模范教师”、“全国优秀教师”6人。1个团队入选首批“全国高校黄大年式教师团队”，3个创新团队入选教育部“长江学者和创新团队发展计划”，2个创新团队入选“山东省优秀创新团队”，1学科入选“山东省泰山学者优势特色学科人才团队支持计划”。','https://www.sdjzu.edu.cn/'),(10,'齐鲁工业大学','齐鲁工业大学（山东省科学院）坐落于国家历史文化名城——泉城济南，是山东省重点建设的应用研究型大学，同时也是山东省最大的综合性自然科学研究机构。学校（科学院）现有长清、彩石、历城、历下、千佛山、菏泽6个校区，在济南、青岛、济宁、临沂、菏泽等地设有研究机构，主校区在济南长清大学科技园。学校（科学院）拥有完善的现代化教学基础设施和实验设备。有省部级以上重点学科及研究平台117个，其中省部共建国家重点实验室1个、省部共建国家地方联合工程实验室1个、国家工程技术研究中心1个、国家超级计算济南中心1个、教育部重点实验室1个、国际科技合作基地3个、国家产业技术创新战略联盟1个、省级协同创新中心3个、省级重点学科9个、省级重点实验室16个、省级工程实验室9个、省工程技术研究中心26个、省工程技术创新中心1个、省级高校重点实验室5个、省产业技术创新战略联盟5个、省国际合作研究中心6个，省级社科基地（中心）3个；有省级实验教学示范中心3个，省级人才培养模式创新实验区1个，智能制造省级新旧动能转换实训基地等学生实践教学和实训基地408个。 校舍建筑总面积123万平方米，教学科研设备总值10.3亿元，图书馆藏书249万册，电子图书148万册。建有山东教育科研网大学科技园网络节点和覆盖全校的千兆以太计算机网络，科学与艺术深度融合的齐鲁陶瓷玻璃博物馆，校园环境优美宁静，人文艺术气息浓郁，为“省级园林化校园示范单位”。学校（科学院）现有全日制在校本科生、研究生、留学生3万余人。设23个教学单位，15家创新研究机构。共有9个省部级重点学科，14个硕士学位授权一级学科，93个硕士学位授权二级学科，9个工程硕士专业学位授权领域，3个艺术硕士专业学位授权领域，2个翻译硕士专业学位授权领域，1个金融硕士专业学位授权领域，75个本科专业。学科专业涵盖工学、理学、文学、经济学、管理学、法学、医学和艺术学等8个门类，化学、工程、材料科学三个学科进入ESI世界学术机构排名前1%','qlgy.jpg','山东','理科','普通本科',522,'现有在职专任教师2016人，副高以上专业技术职务人员1018人，具有博士学位者1032人，占专任教师的51.19%。学校现有双聘院士4人，“万人计划”专家2人，“新世纪百千万人才工程”国家级人选4人，国家有突出贡献的中青年专家1人，中央联系专家1人，全国优秀科技工作者1人，泰山学者攀登计划专家2人，泰山学者特聘教授、专家39人，泰山学者青年专家6人，泰山产业领军人才6人，教育部“新世纪优秀人才”支持计划人选5人，科技部中青年科技创新领军人才2人，山东省有突出贡献的中青年专家37人，山东省省高端智库专家7人，享受国务院政府特殊津贴专家23人，全国师德标兵1人，全国优秀教师1人，“山东省教学名师”6人。在职在岗的各类省部级以上高层次人才191人次。','sdjzu.com');

/*Table structure for table `majors` */

DROP TABLE IF EXISTS `majors`;

CREATE TABLE `majors` (
  `id` int NOT NULL AUTO_INCREMENT,
  `NAME` varchar(32) NOT NULL,
  `describes` text,
  `TYPE` varchar(32) DEFAULT NULL,
  `level` varchar(32) DEFAULT NULL,
  `goal` text,
  `needed` text,
  `courses` varchar(225) DEFAULT NULL,
  `ability` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `majors` */

insert  into `majors`(`id`,`NAME`,`describes`,`TYPE`,`level`,`goal`,`needed`,`courses`,`ability`) values (7,'哲学','哲学研习中西现代哲学、中西哲学史、伦理学、逻辑学、人类学等方面的基本理论和知识，以世界本原为探究对象，研究自然、社会和人类最普遍的问题，探索人的价值观、人生观和世界观的问题。例如：世界的本质是什么，人类应该怎样活着，人类如何看待死亡等。该专业侧重于学术方面，选择需谨慎。关键词：马克思主义三观逻辑伦理','哲学类','本科','哲学专业是培养具有一定马克思主义哲学理论素养和系统的专业基础知识，能运用科学的世界观和方法论分析当代世界与中国的现实问题的应用型、复合型高级专门人才的学科','他们具有一定的哲学理论思维能力、创新能力、口头与文字表达能力、社会活动能力和一定的科研能力，具有较高外语水平的理论研究人才以及能在国家机关、文教事业新闻出版、企业等部门从事实际工作。','哲学概论、马克思主义哲学原理、马克思主义哲学史、中国哲学史、西方哲学史、现代西方哲学、科学技术哲学、伦理学、宗教学、美学、逻辑学、心理学、中外哲学原著导读、马克思主义哲学原著导读等','毕业生应获得以下几方面的知识和能力 :\r\n1.比较系统地掌握马克思主义哲学、中国哲学和西方哲学的理论和历史；2.具有一定的社会科学、人文科学、自然科学、思维科学的相关知识；3.掌握哲学学科的基本研究方法、治学方法和相应的社会调查能力；4.了解国内外哲学界最重要的理论前沿和发展动态；5.了解国内外最重大的实践问题和发展动态；6.具有分析和解决社会现实问题的初步能力。'),(8,'临床医学','临床医学主要研究基础医学、临床医学、手术学等方面的基本知识和技能，进行人类疾病的诊断、治疗、预防等。例如：骨折、心脏病等疾病的诊断，心脏搭桥、器官移植等手术的实施，肿瘤的放射治疗等。关键词：临床医生手术器官移植','临床医学类','本科','临床医学专业是一门实践性很强的应用科学专业，致力于培养具备基础医学、临床医学的基本理论和医疗预防的基本技能；能在医疗卫生单位、医学科研等部门从事医疗及预防、医学科研等方面工作的医学高级专门人才。','临床医学专业学生主要学习医学方面的基础理论和基本知识，人类疾病的诊断、治疗、预防方面的基本训练．具有对人类疾病的病因、发病机制做出分类鉴别的能力。','人体解剖学、组织胚胎学、生理学、生物化学、病理学、诊断学、内科学、外科学、妇产科学、儿科学','毕业生应获得以下几方面的知识和能力 :\r\n1.掌握基础医学中临床医学的基本理论、基本知识；2.掌握常见病名发病诊断处理的临床基本技能；3.具有对急、难、重症的初步处理能力；4.熟悉国家卫生工作方针、政策和法规；5.掌握医学文献检索、资料调查的基本方法，具有一定的科学研究和实际工作能力。'),(9,'工程管理','工程管理主要研究管理学、经济学、信息工程、土木工程等方面的基本知识和技能，在工程建设和房地产等领域进行项目规划、决策、管理、组织等。例如：建筑工程的审计、评价和可行性分析，工程项目全过程的进度管理、质量把控和组织协调，工程项目造价和收益的预估等。关键词：工程管理建筑房地产','管理科学与工程类','本科','本专业培养具有工程技术、管理学和经济学的基本知识，掌握现代管理科学的理论、方法和手段，能在国内外大中型企业、工程技术公司、国际经济合作公司、工程咨询与评估公司等、建设单位、银行、政府建设主管部门、科研与教育部门从事项目决策和全过程管理的（复合型）应用型高级管理人才。','该专业学生主要学习工程管理方面的基本理论、方法和土木工程技术知识；受到工程项目管理方面的基本训练；具备从事工程项目管理的基本能力。','工程图学、工程材料、工程力学、工程结构、运筹学、工程经济学、工程项目管理、建设法规、工程合同法律制度、工程合同管理、工程估价、工程财务管理、工程成本规划与控制、组织行为学、人力资源管理、工程信息管理','毕业生应获得以下几方面的知识和能力 :\r\n1.掌握以土木工程技术为主的理论知识和实践技能；2.掌握相关的管理理论和方法；3.掌握相关的经济理论；4.掌握相关的法律、法规；5.具有从事工程管理的理论知识和实践能力；6.具有阅读工程管理专业外语文献的能力；7.具有运用计算机辅助解决工程管理问题的能力；8.具有较强的科学研究能力。总的来说，工程管理还是偏重于管理科学，适合那些人际交往能力强，又善于用理性去思考问题的考生报考。'),(10,'艺术史论','艺术史论主要研究中外艺术学理论和中外艺术史等方面的基本理论和知识，在各级文化部门、美术馆、博物馆，以及报纸杂志、广播电视、出版机构、文化公司等单位进行策划、管理、编辑、评论与文创等。例如：油画、雕塑等艺术品的创作与鉴赏，诗词、文章的撰写与编辑，艺术品真伪的鉴定等。关键词：油画雕塑水墨画诗词','艺术学理论类','本科','艺术史论专业培养具备中外艺术史与艺术理论等方面的基本知识，能在各级文化部门、美术馆、博物馆，以及报纸杂志、广播电视、出版机构、文化公司等单位工作的复合型人才。','毕业生应获得以下几方面的知识和能力 :\r\n1.掌握艺术学学科的基本理论和基本知识；2.熟悉各个艺术门类的基本知识，掌握艺术学综合的分析方法和分析能力；3.具有艺术鉴赏和艺术批评的基本能力；4.熟悉党和国家关于文化艺术和文化产业的各项方针、政策和法规；5.了解艺术学科的理论前沿、应用前景、发展动态和行业需求；6.具有一定的科学研究和实际工作能力，具有一定的批判性思维能力。','艺术学原理、艺术文化学、艺术心理学、艺术教育学、艺术传播学、中国艺术学、西方艺术学、中国艺术史、亚洲艺术史、西方艺术史','毕业生应获得以下几方面的知识和能力 :\r\n1.掌握艺术学学科的基本理论和基本知识；2.熟悉各个艺术门类的基本知识，掌握艺术学综合的分析方法和分析能力；3.具有艺术鉴赏和艺术批评的基本能力；4.熟悉党和国家关于文化艺术和文化产业的各项方针、政策和法规；5.了解艺术学科的理论前沿、应用前景、发展动态和行业需求；6.具有一定的科学研究和实际工作能力，具有一定的批判性思维能力。'),(11,'经济学','经济学主要研究经济学、金融学、投资学、精算学等方面的基本知识和技能，在银行、证券、信托等金融机构进行经济分析、预测、规划、管理以及各类金融服务。例如：债券、基金的投资前景分析，股票、投资的风险评估，汽车、房子的抵押贷款，企业破产时的资产清算等。关键词：基金债券股票贷款','经济学类','本科','该专业培养具备比较扎实的经济学理论基础，熟悉现代经济学理论，比较熟练地掌握现代经济分析方法，知识面较宽，具有向经济学相关领域扩展渗透的能力，能在综合经济管理部门、政策研究部门，金融机构和企业从事经济分析、预测、规划和经济管理工作的高级专门人才。','该专业要求学生系统掌握经济学基本理论和相关的基础专业知识，了解市场经济的运行机制，熟悉国家的经济方针、政策和法规，了解中外经济发展的历史和现状；了解经济学的学术动态；具有运用数量分析方法和现代技术手段进行社会经济调查、经济分析和实际操作的能力；具有较强的文字和口头表达能力的专门人才，能熟练掌握一门外语。','政治经济学（一般理论、资本主义经济、社会主义经济）、西方经济学（微观经济、宏观经济）、计量经济学、统计学、财政学、货币金融学、会计学、经济史（中国经济史、外国经济史）、经济思想史、当代中国经济','毕业生应获得以下几方面的知识和能力 :\r\n1.掌握马克思主义经济学、当代西方经济学的基本理论和分析方法；2.掌握现代经济分析方法和计算机应用技能；3.了解中外经济学的学术动态及应用前景；4.了解中国经济体制改革和经济发展；5.熟悉党和国家的经济方针、政策和法规；6.掌握中外经济学文献检索、资料查询的基本方法、具有一定的经济研究和实际工作能力。'),(12,'法学','法学主要研究法律、法律现象、法律相关问题等方面的基本理论和知识，涉及宪法、刑法、民商法、经济法、诉讼法等，进行诉讼、法务的处理等。例如：离婚、遗产纠纷等民事诉讼的辩护，谋杀、盗窃等刑事案件的检控，企业合同的拟定、审查、修改，劳务纠纷的协调处理等。关键词：律师宪法法院检察院','法学类','本科','法学专业培养系统掌握法学知识，熟悉我国法律和党的相关政策，能在国家机关、企事业单位和社会团体、特别是能在立法机关、行政机关、检察机关、审判机关、仲裁机构和法律服务机构从事法律工作的高级专门人才。','本专业培养的学生需要具备法学的基本理论和基本知识，受到法学思维和法律实务的基本训练，具有运用法学理论和方法分析问题和运用法律管理事务与解决问题的基本能力。','法理学、宪法学、民法学、商法学、知识产权法、刑法学、民事诉讼法学、刑事诉讼法学、行政法与行政诉讼法、国际法、国际私法、国际经济法、环境资源法、劳动与社会保障法','毕业生应获得以下几方面的知识和能力 :\r\n1.掌握法学各学科的基本理论与基本知识；2.掌握法学的基本分析方法和技术；3.了解法学的理论前沿和法制建设的趋势；4.熟悉我国法律和党的相关政策；5.具有运用法学知识去认识问题和处理问题的能力；6.掌握文献检索、资料查询的基本方法，具有一定的科学研究和实际工作的能力。'),(13,'教育学','教育学主要研究教育学、心理学等方面的基本理论和知识，包括教育现象、教育问题、教育规律、教育方法、学生心理状态等，培养良好的人文素养和教师职业素养，进行教育教学、教育研究和教育管理等。例如：对于高效教学方法的研发、对于不同年龄段学生心理状态的研究等。关键词：学校教育教师班主任','教育学类','本科','本专业培养具有良好思想道德品质、较高教育理论素养和较强教育实际工作能力的中、高等师范院校师资、中小学校教育科研人员、教育科学研究单位研究人员、各级教育行政管理人员和其他教育工作者。','本专业学生主要学习教育科学的基本理论和基本知识，受到教育科学研究的基本训练，掌握从事教师工作的基本技能。','毕业生应获得以下几方面的知识和能力 :','普通心理学、发展心理学、教育心理学、教育概论、教学论、课程论、德育原理、教育社会学、中国教育史、外国教育史等'),(14,'汉语言文学','汉语言文学主要研究汉语和中国文学等方面的基本理论和知识，包括其相关理论、发展历史和研究现状等，涉及诗词歌赋、现当代文学、民间文学等多个领域，在报刊宣传、新闻出版行业进行文章编辑与撰写、文学评论等。常见的汉语言文学研究内容有：婉约派、豪放派等文学流派，杜甫、李白等古代诗人，鲁迅、朱自清等现当代作家等。相较于汉语言，汉语言文学侧重于研究文学的修养与内涵，汉语言侧重于语言研究。关键词：杜甫苏轼将进酒朝花夕拾','中国语言文学类','本科','汉语言文学专业培养具有汉语言文学基本理论、基础知识和基本技能，能在新闻文艺出版部门、科研机构和机关企事业单位从事文学评论、汉语言文学教学与研究工作，以及文化、宣传方面的实际工作的汉语言文学专门人才。','该专业学生主要学习汉语和中国文学方面的基本知识，受到有关理论、发展历史、研究现状等方面的系统教育和业务能力的基本训练。','文学概论、中国古代文学（含中国古代文学史、中国古代文学作品选）、中国现代文学（含中国现代文学史、中国现代文学作品选、中国当代文学）、外国文学、语言学概论、古代汉语、现代汉语、写作','毕业生应获得以下几方面的知识和能力 :\r\n1.掌握马克思主义的基本原理和关于语言、文学的基本理论；2.掌握本专业的基础知识以及新闻、历史、哲学、艺术、教育等学科的相关知识；3.具有文学修养、鉴赏文学能力、较强的写作能力以及语言表达能力；4.了解我国关于语言文字和文学艺术的方针、政策和法规；5.了解本学科的前沿成就和发展前景；6.能阅读古典文献，掌握文献检索、资料查询的基本方法，具有一定的科学研究和实际工作能力。7.具有正确的文艺观点、语言文字观点和坚实的汉语言文学基础知识，并具有处理古今语言文字材料的能力、解读和分析古今文学作品的能力、协作能力和设计实施语文教学的能力。'),(15,'历史学','历史学主要研习马克思主义基本原理、历史学、古文字学、历史地理学等方面的基本理论和知识，涉及中外通史和断代史、中西史学史、中外历史文献选读等，对先人遗留下来的古籍、文献、历法、制度进行研究分析，从而为当今的社会生活所借鉴。该专业侧重于理论研究，对口的职业相对需求较少，选择需谨慎。关键词：史记资治通鉴战国策汉书','历史学类','本科','历史学专业培养具有一定的马克思主义基本理论素养和系统的专业基本知识，有进一步培养潜能的史学专门人才，以及能在国家机关、文教事业、新闻出版、文博档案及各类介事业单位从事实际工作的应用型、复合型高级专门人才。','该专业学生主要学习历史科学的基本理论和基本知识，受到中国历史和世界历史发展的基本史实及史学研究的基本训练，具有从事专业工作所需的基本能力','中国通史、世界通史、史学概论、中国历史要籍介绍及选读、外国历史要籍介绍及选读、中国史学史、西方史学史等','毕业生应获得以下几方面的知识和能力 :\r\n1．掌握历史学科的基本理论和基础知识，对社会科学、人文科学与自然科学等相关学科有一定的了解；2．掌握历史学的基本研究方法与分析方法；3．具有从事历史研究的初步能力和较强的口头表达和文字表达能力；4．熟悉古文字学、版本目录学、音韵学、史料学、历史地理学及考古学等方面的基础知识；5．了解国内外史学界景重要的理论学术前沿和发展动态趋势；6．掌握文献检索，资料查询的基本方法技能。'),(16,'物理学','物理学主要研究物质运动规律和物质基本结构，是关于大自然规律的知识，探索分析大自然所发生的现象，以了解其形成原因及过程，例如：闪电、雨水的形成原因及过程，宇宙行星的运转规律、生活中电路短路断路的原因。关键词：理论粒子闪电电路','物理学类','本科','物理学专业培养掌握物理学的基本理论与方法，具有良好的数学基础和实验技能，能在物理学或相关的科学技术领域中从事科研、教学、技术和相关的管理工作的高级专门人才。','该专业学生主要学习物质运动的基本规律，接受运用物理知识和方法进行科学研究和技术开发训练，获得基础研究或应用基础研究的初步训练，具备良好的科学素养和一定的科学研究与应用开发能力。','示例一：力学（68学时）、热学（51学时）、电磁学（51学时）、光学（51学时）、近代物理（51学时）、原子核物理（68学时）','毕业生应获得以下几方面的知识和能力 :\r\n1.掌握数学的基本理论和基本方法，具有较高的数学修养；2.掌握坚实的、系统的物理学基础理论及较广泛的物理学基本知识和基本实验方法，具有一定的基础科学研究能力和应用开发能力；3.了解相近专业的一般原理和知识；4.了解物理学发展的前沿和科学发展的总体趋势；5.了解国家科学技术、知识产权等有关政策和法规；6.掌握资料查询、文献检索及运用现代信息技术获取相关信息的基本方法；具有-定的实验设计，创造实验条件，归纳、整理、分析实验结果，撰写论文，参与学术交流的能力。'),(17,'软件工程','软件工程是一门研究用工程化方法构建和维护有效、实用和高质量的软件的学科。它涉及程序设计语言、数据库、软件开发工具、系统平台、标准、设计模式等方面。在现代社会中，软件应用于多个方面。典型的软件有电子邮件、嵌入式系统、人机界面、办公套件、操作系统、编译器、数据库、游戏等。同时，各个行业几乎都有计算机软件的应用，如工业、农业、银行、航空、政府部门等。这些应用促进了经济和社会的发展，也提高了工作效率和生活效率 。','计算机类','本科','本专业是培养适应计算机应用学科的发展，特别是软件产业的发展，具备计算机软件的基础理论、基本知识和基本技能，具有用软件工程的思想、方法和技术来分析、设计和实现计算机软件系统的能力，毕业后能在IT行业、科研机构、企事业中从事计算机应用软件系统的开发和研制的高级软件工程技术人才。','本专业主要学习软件工程方法、软件系统开发、系统分析与设计、软件开发管理、软件测试、软件质量保证等知识，要求重点掌握软件需求分析、开发、项目管理和软件测试等一系列相关专业知识。','示例一（括号内为理论授课+实验学时数）：离散数学（64学时）、计算系统基础（64+48学时）、计算与软件工程I（个人级软件开发）（48+48学时）、计算与软件工程Ⅱ（小组级软件开发）（48+48学时）、计算与软件工程Ⅲ（团队软件工程实践）（16+96学时）、数据结构与算法（64+48学时）、操作系统（48+48学时）、计算机网络（48+48学时）','毕业生应获得以下几方面的知识和能力 :\r\n1.掌握和计算机科学与技术相关的基本理论知识；2.掌握计算机系统的分析和设计的基本方法；3.了解文献检索、资料查询的基本方法，具有一定的科学研究和实际工作能力；4.了解与计算机有关的法规；5.能够运用学习知识和外文阅读能力查阅外文资料；6.掌握文献检索、资料查询的基本方法，具有获取信息的能力。'),(18,'网络工程','网络工程主要研究计算机软硬件、网络与通信系统等方面的基本知识和技术，进行计算机网络系统的规划设计、维护管理和应用开发等。例如：企业网站的设计开发，游戏服务器的维护，网络故障（如：无法连接网络）的原因排查，木马等网络病毒的处理等。关键词：计算机互联网服务器木马病毒','计算机类','本科','网络工程专业是讲计算机科学基础理论、计算机软硬件系统及应用知识、网络工程的专业知识及应用知识。网络工程专业具有创新意识，具有本专业领域分析问题和解决问题的能力，具备一定的实践技能，并具有良好的外语应用能力的高级研究应用型专门人才。','该专业学生主要学习计算机、通信以及网络方面的基础理论、设计原理，掌握计算机通信和网络技术，接受网络工程实践的基本训练，具备从事计算机网络设备、系统的研究、设计、开发、工程应用和管理维护的基本能力。','示例一：计算机原理（58+6学时）、计算机程序设计（28+8学时）、数据结构（40+6学时）、操作系统（46+8学时）、计算机网络（44+10学时）、信息安全导论（30+6学时）、数据通信(32+4学时)、互联网协议分析与设计（30+6学时）、网络工程（32+4学时）','毕业生应获得以下几方面的知识和能力 :\r\n1.具有扎实的自然科学基础、较好的人文社会科学基础和外语综合能力；2.系统地掌握计算机和网络通信领域内的基本理论和基本知识；3.掌握计算机、网络与通信系统的分析、设计与开发方法；4.具有设计、开发、应用和管理计算机网络系统的基本能力；5.了解计算机及网络通信领域的一些最新进展与发展动态；6.了解信息产业、计算机网络建设及安全的基本方针、政策和法规；7.掌握文献检索、资料查询的基本方法，具有一定的科学研究和实际工作能力。'),(19,'计算机科学与技术','计算机科学与技术主要研究计算机的设计与制造，包含计算机软件、硬件的基本理论、技能与方法，进行计算机系统和软件的开发与维护、硬件的组装等。例如：Windows系统的维护，手机APP的开发，台式电脑的整机装配等。相较于网络工程、软件工程，计算机科学与技术专业所学范围更广。关键词：计算机Windows软件电脑主机','计算机类','本科','该专业主要培养具有良好的科学素养，系统地、较好地掌握计算机科学与技术包括计算机硬件、软件与应用的基本理论、基本知识和基本技能与方法，能在科研部门、教育单位、企业、事业、技术和行政管理部门等单位从事计算机教学、科学研究和应用的计算机科学与技术学科的高级科学技术人才。','该专业学生主要学习计算机科学与技术方面的基本理论和基本知识，接受从事研究与应用计算机的基本训练，具有研究和开发计算机系统的基本能力。','示例一：高级语言程序设计（40+48学时）、计算机导论（24+6学时）、集合论与图论（48学时）、汇编语言程序设计（32+8学时）、电路44+16学时）、数理逻辑（32学时）、电子技术基础(32+20学时)、数字逻辑设计（36+12学时）、数据结构与算法（40+24学时）','毕业生应获得以下几方面的知识和能力 :\r\n1.具备扎实的数据基础理论和基础知识；2.具有较强的思维能力、算法设计与分析能力；3.系统掌握计算机科学与技术专业基本理论、基本知识和操作技能；4.了解学科的知识结构、典型技术、核心概念和基本工作流程；5.有较强的计算机系统的认知、分析、设计、编程和应用能力；6.掌握文献检索、资料查询的基本方法、能够独立获取相关的知识和信息，具有较强的创新意识；7.熟练掌握一门外语，能够熟读该专业外文书刊。'),(20,'土木工程','土木工程主要研究各类土地工程设施的勘测、设计、建造、保养、维修等方面的基本知识和技术，进行各类工程建筑物的新建、改建或扩建，以及相关配套设施的勘察、规划、设计、施工等。主要工程设施包含房屋、道路、铁路、管道、隧道、桥梁、堤坝、矿井等。关键词：土地建筑桥梁施工','土木类','本科','土木工程专业培养掌握工程力学、流体力学、岩土力学和市政工程学科的基本理论和基本知识，具备从事土木工程的项目规划、设计、研究开发、施工及管理的能力，能在房屋建筑、地下建筑、隧道、道路、桥梁、矿井等的设计、研究、施工、教育、管理、投资、开发部门从事技术或管理工作的高级工程技术人才。','该专业学生主要学习工程力学、岩土工程、结构工程、市政工程、给水排水工程和水利工程学科的基本理论和知识，受到工程制图、工程测量、计算机应用、专业实验、结构设计及施工实践等方面的基本训练，以及具备从事建筑工程、交通土建工程、水利水电工程、港口工程、海岸工程和给水排水工程的规划、设计、施工、管理及相关研究工作的能力。','示例一：工程力学（119学时）、结构力学（102学时）、流体力学（34学时）、土力学（34学时）、弹性力学（34学时）、土木工程材料（34学时）、土木工程概论（17学时）、工程地质（34学时）、画法几何与工程制图（68学时）、测量学（51学时）、土木工程法规（17学时）','毕业生应获得以下几方面的知识和能力 :\r\n1.具有较扎实的自然科学基础，较好的人文社会科学基础和外语语言综合能力2.掌握工程力学、流体力学、岩土力学工程地质学和工程制图的基本理论与基本知识3.掌握建筑材料、结构计算、构件设计、地基处理、给水排水工程和计算机应用方面的基本知识、原理、方法与技能，初步具有从事土建结构工程的设计与研究工作的能力4.掌握建筑机械、电工学、工程测量、施工技术与施工组织、工程监测、工程概预算及工程招标等方面的基本知识、基本技能，初步具有从事工程施工、管理和研究工作的能力5.熟悉各类土木工程的建设方针、政策和法规6.了解土木工程各主干学科的理论前沿和发展动态7.掌握文献检索和资料查询的基本方法，具有一定的科学研究和实际工作能力。'),(21,'建筑学','建筑学主要研究建筑的结构、设计、材料、环境、建造等方面的基本知识和技能，涉及建筑设计、城市设计、室内设计、市政设计等多个方面，进行各类建筑的设计和规划等。例如：大厦的建造方案和施工图的设计，城市大型建筑分布的布局规划，房屋室内的装饰设计等。关键词：建筑绘图设计住宅','建筑类','本科','本专业培养具备建筑设计、城市设计、室内设计等方面的知识，能在设计部门从事设计工作，并具有多种职业适应能力的通用型、复合型高级工程技术人才。','建筑学专业学生主要学习建筑设计、城市规划原理、建筑工程技术、环境和空间表现、绘画艺术等方面的基本理论与基本知识，受到建筑设计等方面的基本训练，具有项目策划、建筑设计方案和建筑施工图绘制等方面的基本能力。','示例一：建筑设计（832学时）、外国建筑史（64学时）、中国建筑史（64学时）、建筑设计概论（48学时）、建筑设计基础（32学时）、城市规划原理（48学时）、景观学原理（32学时）、空间形体表达基础（32学时）、建筑技术概论（16学时）、CAAD方法（32学时）、建筑结构（64学时）、建筑物理环境（48学时）、素描水彩（192学时）、建筑师业务实践（208学时）','毕业生应获得以下几方面的知识和能力 :\r\n1.具有较扎实的自然科学基础、较好的人文社会科学基础和外语语言综合能力；2.掌握建筑设计的基本原理和方法，具有独立进行建筑设计和用多种方式表达设计意图的能力以及具有初步的计算机文字、图形、数据的处理能力；3.了解中外建筑历史的发展规律，掌握人的生理、心理、行为与建筑环境的关系，与建筑有关的经济知识、社会文化习俗、法律与法规的基本知识，以及建筑边缘学科与交叉学科的相关知识；4.初步掌握建筑结构及建筑设备体系与建筑的安全、经济、适用、美观的关系的基本知识，建筑构造的原理与方法，常用建筑材料及新材料的性能。具有合理选用和一定的综合应用能力，并具有一定的多工种间组织协调能力；5.具有项目前期策划、建筑设计方案和建筑施工图绘制的能力，具有建筑美学的修养。');

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `role` */

insert  into `role`(`id`,`name`) values (1,'administrator'),(2,'user');

/*Table structure for table `types` */

DROP TABLE IF EXISTS `types`;

CREATE TABLE `types` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `type` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `types` */

insert  into `types`(`id`,`name`,`type`) values (1,'哲学类','哲学'),(2,'建筑学类','理学'),(3,'计算机类','工学');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `NAME` varchar(32) NOT NULL,
  `PASSWORD` varchar(32) NOT NULL,
  `TYPE` varchar(32) DEFAULT NULL,
  `score` int DEFAULT NULL,
  `ranks` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`NAME`,`PASSWORD`,`TYPE`,`score`,`ranks`) values (1,'Ranger','123456','物理,化学,生物',680,1695),(2,'administrator','123456','物理,化学,生物',750,1),(13,'李杰','123456','物理,化学,生物',543,75364),(14,'王兴胜','123456','生物,化学,物理',542,75364),(15,'周经纬','123456','物理,化学,生物',542,73364),(16,'刘宇航','123456','物理,化学,生物',542,73364),(17,'朱佳喜','123456','物理,化学,生物',542,73364),(18,'李永琪','123456','物理,化学,生物',542,73364),(19,'朱令壮','123456','物理,化学,生物',542,73364),(20,'乔寿凯','123456','物理,化学,生物',542,73364),(21,'陈德友','123456','物理,化学,生物',542,73364),(22,'王浩玮','123456','物理,化学,生物',542,73364),(23,'李师昊','123456','物理,化学,生物',542,73364),(24,'庞晓辉','123456','物理,化学,生物',542,73364),(25,'张永琪','123456','物理,化学,生物',542,73364),(26,'杨朝晖','123456','物理,化学,生物',542,73364),(27,'郭庆','123456','物理,化学,生物',542,73364);

/*Table structure for table `users_role` */

DROP TABLE IF EXISTS `users_role`;

CREATE TABLE `users_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` int NOT NULL,
  `rid` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `users_role` */

insert  into `users_role`(`id`,`uid`,`rid`) values (1,2,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
