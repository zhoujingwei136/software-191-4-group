import Vue from 'vue'
import Router from 'vue-router'

import Login from './views/Login.vue'
import Register from './views/Register'
import University from './views/University'
import Universities from "./views/Universities";
import Major from './views/Major'
import Ranking from "./views/Ranking";
import Index from "./views";
import Majors from "./views/Majors";
import Recommend from "./views/Recommend";

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '',
      redirect: '/login'
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/register',
      component: Register
    },
    {
      path: '/universities',
      component: Universities
    },
    {
      path: '/major/:majorName',
      component: Major
    },
    {
      path: '/ranking',
      component: Ranking
    },
    {
      path: '/index',
      component: Index
    },
    {
      path: '/university/:collegeId',
      component: University
    },
    {
      path: '/majors',
      component: Majors
    },
    {
      path: '/recommend',
      component: Recommend
    }
  ]
})
